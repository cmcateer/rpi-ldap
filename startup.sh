#!/bin/bash

# https://github.com/moby/moby/issues/8231#issuecomment-63871343
ulimit -n 1024

source common.sh

function cleanup {
    /bin/bash backup.sh
}

# Passwords
if [ ! -z $LDAP_ADMIN_PWD_FILE -a -f $LDAP_ADMIN_PWD_FILE ]; then
    LDAP_ADMIN_PWD=`cat $LDAP_ADMIN_PWD_FILE`;
fi

echo "Configuring slapd install"
configure_slapd

echo "Running initial setup"
init_server

echo "Registering shutdown backup hook"
trap cleanup SIGTERM

echo "Restarting server"
run_slapd &

wait $!
