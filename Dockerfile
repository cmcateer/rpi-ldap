FROM debian:buster-slim

RUN groupadd -r openldap \
    && useradd -r -g openldap -u 999 openldap \
    && apt-get -y update \
    && LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes --no-install-recommends \
        procps slapd ldap-utils \
    && apt-get clean

COPY data /tmp/data/
COPY common.sh startup.sh backup.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/startup.sh /usr/local/bin/backup.sh

VOLUME ["/etc/ldap"]

CMD ["/usr/local/bin/startup.sh"]

EXPOSE 389 636
