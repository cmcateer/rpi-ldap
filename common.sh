BACKUP_DIR=/var/lib/ldap/backup

function replace {
    echo $1
    sed -i "s/\${LDAP_ORGANIZATION}/${LDAP_ORGANIZATION}/g" $1
    sed -i "s/\${LDAP_EXTENSION}/${LDAP_EXTENSION}/g" $1
    sed -i "s/\${LDAP_DOMAIN}/${LDAP_DOMAIN}/g" $1
}

function configure_slapd {
    echo slapd slapd/internal/generated_adminpw password ${LDAP_ADMIN_PWD} | debconf-set-selections \
        && echo slapd slapd/internal/adminpw password ${LDAP_ADMIN_PWD} | debconf-set-selections \
        && echo slapd slapd/password2 password ${LDAP_ADMIN_PWD} | debconf-set-selections \
        && echo slapd slapd/password1 password ${LDAP_ADMIN_PWD} | debconf-set-selections \
        && echo slapd slapd/dump_database_destdir string /var/backups/slapd-VERSION | debconf-set-selections \
        && echo slapd slapd/domain string ${LDAP_DOMAIN} | debconf-set-selections \
        && echo slapd shared/organization string ${LDAP_ORGANIZATION} | debconf-set-selections \
        && echo slapd slapd/purge_database boolean true | debconf-set-selections \
        && echo slapd slapd/move_old_database boolean true | debconf-set-selections \
        && echo slapd slapd/allow_ldap_v2 boolean false | debconf-set-selections \
        && echo slapd slapd/no_configuration boolean false | debconf-set-selections \
        && echo slapd slapd/dump_database select when needed | debconf-set-selections \
        && dpkg-reconfigure -f noninteractive slapd

}

function run_slapd {
    DEBUG_LEVEL=256 # connections, LDAP operations, results
    /usr/sbin/slapd -h 'ldap:/// ldapi:///' -g openldap -u openldap -F /etc/ldap/slapd.d -d ${DEBUG_LEVEL}
    echo "done....> $?"
}

function init_server {
    echo "Starting server"
    run_slapd &
    for i in {30..0}; do
        ldapsearch -x -w ${LDAP_ADMIN_PWD} -D cn=admin,dc=${LDAP_DOMAIN},dc=${LDAP_EXTENSION} -b dc=${LDAP_DOMAIN},dc=${LDAP_EXTENSION} -LLL # &> /dev/null
        r="$?"
        echo result $r
        # TODO: it returns 49, Bad Credentials,
        # but as long as it's not 255 (Can't contact), it's started
        #if [ "$r" -eq 0 ]; then
        if [ "$r" -ne 255 ]; then
            break
        fi
        echo 'LDAP init process in progress...'
        sleep 1
    done
    if [ "$i" = 0 ]; then
        echo >&2 'LDAP init process failed.'
        exit 1
    fi

    CONFIG_BACKUP=$(get_latest_backup config)
    DATA_BACKUP=$(get_latest_backup data)

    if [ -f "$DATA_BACKUP" ]; then
        echo "Found data backup"
        echo "Loading $DATA_BACKUP"
        slapadd -F /etc/ldap/slapd.d -l "$DATA_BACKUP"
    else
        echo "Initializing server with new data"
        for i in `ls /tmp/data/[^_]*.ldif`; do
            replace $i
            ldapadd -w ${LDAP_ADMIN_PWD} -D "cn=admin,dc=${LDAP_ORGANIZATION},dc=${LDAP_EXTENSION}" -f $i
        done;
    fi

    if [ -f "$CONFIG_BACKUP" ]; then
        echo "Found config backup"
        echo "Loading $CONFIG_BACKUP"
        slapadd -F /etc/ldap/slapd.d -l "$CONFIG_BACKUP"
    else
        echo "Initializing server with new config"
        # Del 1 ACL
        ldapmodify  -Y EXTERNAL -H ldapi:/// -f /tmp/data/_acl_del.ldif;
        # Add 2 ACLs
        replace /tmp/data/_acl_add_0.ldif;
        ldapmodify  -Y EXTERNAL -H ldapi:/// -f /tmp/data/_acl_add_0.ldif
        replace /tmp/data/_acl_add_1.ldif;
        ldapmodify  -Y EXTERNAL -H ldapi:/// -f /tmp/data/_acl_add_1.ldif
    fi

    echo "Stopping server"
    pid=$(ps -U openldap -o pid=)
    if [ ! -z "$pid" ] && ! kill -s TERM "$pid" ; then
        echo >&2 'LDAP stop process failed.'
        #exit 1
    fi

    rm -Rf /tmp/data
}

function remove_backup_files {
    BACKUP_FILE=$1
    BACKUPS_TO_KEEP=${2:-5}

    ls -tr1 $BACKUP_DIR/$BACKUP_FILE-* | head -n -$BACKUPS_TO_KEEP | xargs -L1 echo deleting: 
    ls -tr1 $BACKUP_DIR/$BACKUP_FILE-* | head -n -$BACKUPS_TO_KEEP | xargs -L1 rm -f
}

function backup {
    mkdir $BACKUP_DIR

    case "$1" in

        config)
        slapcat -b cn=config -F /etc/ldap/slapd.d -l $BACKUP_DIR/config-$(date +%F_%T).ldif
        ;;

        data)
        slapcat -F /etc/ldap/slapd.d -l $BACKUP_DIR/data-$(date +%F_%T).ldif
        ;;

    esac

    # slapcat -n $DB_NUM -l $BACKUP_DIR/$1-$(date +%F_%T).ldif
    echo "Removing extra $1 backups"
    remove_backup_files $1
}

function get_latest_backup {
    ls -t1 $BACKUP_DIR/$1-* | head -n 1
}
